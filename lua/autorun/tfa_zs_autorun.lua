if CLIENT then
	killicon.Add( "tfa_zs_spinfusor", "vgui/killicons/tfa_zs_spinfusor", Color( 255, 255, 255, 255 ) )
	killicon.Add( "tfa_zs_medicbomb", "vgui/killicons/tfa_zs_medicbomb", Color( 255, 255, 255, 255 ) )
	killicon.Add( "tfa_zs_hurricane", "vgui/killicons/tfa_zs_hurricane", Color( 255, 255, 255, 255 ) )
	killicon.Add( "tfa_zs_hyena", "vgui/killicons/tfa_zs_hyena", Color( 255, 255, 255, 255 ) )
	killicon.Add( "tfa_zs_waraxe", "vgui/killicons/tfa_zs_waraxe", Color( 255, 255, 255, 255 ) )
	killicon.Add( "tfa_zs_gladiator", "vgui/killicons/tfa_zs_gladiator", Color( 255, 255, 255, 255 ) )
	killicon.Add( "tfa_zs_jackhammer", "vgui/killicons/tfa_zs_jackhammer", Color( 255, 255, 255, 255 ) )
	killicon.Add( "tfa_zs_bloodbomb", "vgui/killicons/tfa_zs_bloodbomb", Color( 255, 255, 255, 255 ) )
	killicon.Add( "tfa_zs_seditionist", "vgui/killicons/tfa_zs_seditionist", Color( 255, 255, 255, 255 ) )
	
	killicon.Add( "proj_disc", "vgui/killicons/tfa_zs_spinfusor", Color( 255, 255, 255, 255 ) )
	killicon.Add( "proj_bomb_sticky", "vgui/killicons/tfa_zs_hyena", Color( 255, 255, 255, 255 ) )
end

if SERVER then
	--resource.AddWorkshop("") -- Update on workshop release
	resource.AddFile( "materials/nodraw.vmt" )
	resource.AddFile( "materials/nodraw.vtf" )
	
	resource.AddFile( "materials/entities/tfa_zs_medicbomb.png" )
	resource.AddFile( "materials/entities/tfa_zs_spinfusor.png" )
	resource.AddFile( "materials/entities/tfa_zs_hurricane.png" )
	resource.AddFile( "materials/entities/tfa_zs_hyena.png" )
	
	resource.AddFile( "materials/vgui/killicons/tfa_zs_medicbomb.vmt" )
	resource.AddFile( "materials/vgui/killicons/tfa_zs_medicbomb.vtf" )
	resource.AddFile( "materials/vgui/killicons/tfa_zs_spinfusor.vmt" )
	resource.AddFile( "materials/vgui/killicons/tfa_zs_spinfusor.vtf" )
	resource.AddFile( "materials/vgui/killicons/tfa_zs_hurricane.vmt" )
	resource.AddFile( "materials/vgui/killicons/tfa_zs_hurricane.vtf" )
	resource.AddFile( "materials/vgui/killicons/tfa_zs_hyena.vmt" )
	resource.AddFile( "materials/vgui/killicons/tfa_zs_hyena.vtf" )
	resource.AddFile( "materials/vgui/killicons/tfa_zs_gladiator.vmt" )
	resource.AddFile( "materials/vgui/killicons/tfa_zs_gladiator.vtf" )
	resource.AddFile( "materials/vgui/killicons/tfa_zs_jackhammer.vmt" )
	resource.AddFile( "materials/vgui/killicons/tfa_zs_jackhammer.vtf" )
	resource.AddFile( "materials/vgui/killicons/tfa_zs_waraxe.vmt" )
	resource.AddFile( "materials/vgui/killicons/tfa_zs_waraxe.vtf" )
	resource.AddFile( "materials/vgui/killicons/tfa_zs_bloodbomb.vmt" )
	resource.AddFile( "materials/vgui/killicons/tfa_zs_bloodbomb.vtf" )
	resource.AddFile( "materials/vgui/killicons/tfa_zs_seditionist.vmt" )
	resource.AddFile( "materials/vgui/killicons/tfa_zs_seditionist.vtf" )
	
	resource.AddFile( "sound/weapons/zs_glad/gladshot4.wav" )
end

--CreateConVar("tfa_zs_hyena_nolimit", 0, {FCVAR_REPLICATED}, "Disable the bomb limit on the Hyena? 1 Enables, 0 Disables. Goes great with sv_tfa_reloads_enabled 0!") uncomment when fixed