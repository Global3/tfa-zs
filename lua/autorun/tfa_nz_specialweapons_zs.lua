local function RegisterZSSpecialWeps()
    if nzSpecialWeapons == nil then return end

    print("[TFA NZ] Registering ZS Knives")
    --nzSpecialWeapons:AddKnife("tfa_cso_stormgiant", true, 1.5, 1.0)

    print("[TFA NZ] Registering ZS Grenades")
	nzSpecialWeapons:AddGrenade( "nz_bloodnade", 4, false, 0.85, false, 0.4 ) -- ALWAYS pass false instead of nil or it'll assume default value
    --nzSpecialWeapons:AddSpecialGrenade("tfa_cso_chaingrenade", 3, false, 1.5, false, 0.4)
end
hook.Add("InitPostEntity", "nzRegisterSpecialWepsTFAZS", RegisterZSSpecialWeps)