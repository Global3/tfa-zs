AddCSLuaFile()

function AccessorFuncDT(tab, membername, type, id) -- shamelessly ripped out of the zs gamemode
	local emeta = FindMetaTable("Entity")
	local setter = emeta["SetDT"..type]
	local getter = emeta["GetDT"..type]

	tab["Set"..membername] = function(me, val)
		setter(me, id, val)
	end

	tab["Get"..membername] = function(me)
		return getter(me, id)
	end
end

ENT.Type = "anim"

ENT.IgnoreBullets = true
ENT.ChargeTime = 3

AccessorFuncDT(ENT, "HitTime", "Float", 0)
AccessorFuncDT(ENT, "TimeCreated", "Float", 1)

function ENT:ShouldNotCollide(ent)
	return ent:IsPlayer()
end

function ENT:GetCharge()
	if self:GetTimeCreated() == 0 then return 0 end

	return math.Clamp(0.5 + (CurTime() - self:GetTimeCreated()) / self.ChargeTime / 2, 0, 1)
end

util.PrecacheModel("models/combine_helicopter/helicopter_bomb01.mdl")

if CLIENT then
	ENT.NextEmit = 0

	function ENT:Initialize()
		self:SetModelScale(0.2, 0)
		self:SetMaterial("models/props_combine/masterinterface01c")
		self:DrawShadow(false)
	end

	local matGlow = Material("sprites/glow04_noz")
	function ENT:Draw()
		local alt = self:GetDTBool(0)
		local charge = self:GetCharge()
		local c = Color(alt and 100 or 255 * charge, alt and 205 or 0 * charge, alt and 205 or 0 * charge)
		self:SetColor(c)
		self:DrawModel()

		local pos = self:GetPos()
		local size = math.abs((self:GetCharge() == 1 and 1 or 0) * 34 * math.sin(CurTime() * 12))

		render.SetMaterial(matGlow)
		render.DrawSprite(pos, size, size, c)
	end
end

if SERVER then
	function ENT:Initialize()
		self:SetModel("models/combine_helicopter/helicopter_bomb01.mdl")
		self:SetColor(Color(255, 0, 0))
		self:SetMaterial("models/props_combine/masterinterface01c")
		self:PhysicsInitSphere(2)
		self:SetSolid(SOLID_VPHYSICS)
		self:SetModelScale(0.2, 0)
		local phys = self:GetPhysicsObject()
		if phys:IsValid() then
			phys:SetMass(1)
			phys:SetBuoyancyRatio(0.0001)
			phys:EnableMotion(true)
			phys:EnableGravity(true)
			phys:EnableDrag(false)
			phys:Wake()
		end

		self:SetTrigger(true)
		self:DrawShadow(false)
		self:SetUseType(SIMPLE_USE)
		self:SetCollisionGroup(COLLISION_GROUP_DEBRIS)
		self:SetTimeCreated(CurTime())
		self.PostOwner = self:GetOwner() --GetOwner sets to nil on remove
	end

	function ENT:OnRemove()
		if not self.Exploded and not self.PickedUp then
			self:Explode()

			local pos = self:GetPos()
			local alt = self:GetDTBool(0)

			local effectdata = EffectData()
				effectdata:SetOrigin(pos)
			util.Effect("Explosion", effectdata)
			if alt then
				util.Effect("explosion_cold", effectdata)
			end
		end
	end

	function ENT:Use(activator, caller)
		if not self.Exploded and activator:Team() == TEAM_HUMAN and (activator == self:GetOwner() or not self:GetOwner():IsValid()) then
			self.Exploded = true
			self.PickedUp = true

			activator:GiveAmmo(1, "impactmine")

			net.Start("zs_ammopickup")
				net.WriteUInt(1, 16)
				net.WriteString("impactmine")
			net.Send(activator)

			self:Remove()
		end
	end

	function ENT:Think()
		--if self.Stuck and (not self.Stuck:IsValid() or self.Stuck:IsPlayer()) then
		--	self:Explode()
		--end
		if self.Exploded and not self.PickedUp then
			local pos = self:GetPos()
			local alt = self:GetDTBool(0)

			local effectdata = EffectData()
				effectdata:SetOrigin(pos)
			util.Effect("Explosion", effectdata)
			if alt then
				util.Effect("explosion_cold", effectdata)
			end

			self:Remove()
		end
		if self.PhysicsData then
			self:Hit(self.PhysicsData.HitPos, self.PhysicsData.HitNormal, self.PhysicsData.HitEntity, self.PhysicsData.OurOldVelocity)
		end

		self:NextThink(CurTime())
		return true
	end

	function ENT:Hit(vHitPos, vHitNormal, eHitEntity, vOldVelocity)
		if self:GetHitTime() ~= 0 then return end
		self:SetHitTime(CurTime())

		vHitPos = vHitPos or self:GetPos()
		vHitNormal = (vHitNormal or Vector(0, 0, -1)) * -1

		local phys = self:GetPhysicsObject()
		if phys:IsValid() then
			phys:EnableMotion(false)
			phys:Sleep()
		end

		self:SetMoveType(MOVETYPE_NONE)
		self:SetPos(vHitPos + vHitNormal)

		if eHitEntity:IsValid() then
			self:SetSolid(SOLID_NONE)
			self:AddEFlags(EFL_SETTING_UP_BONES)
			self.Stuck = eHitEntity

			local followed = false
			local bonecount = eHitEntity:GetBoneCount()
			if bonecount and bonecount > 1 then
				local boneindex = eHitEntity:NearestBone(vHitPos)
				if boneindex and boneindex > 0 then
					self:FollowBone(eHitEntity, boneindex)
					self:SetPos(eHitEntity:GetBonePositionMatrixed(boneindex))
					followed = true
				end
			end
			if not followed then
				self:SetParent(eHitEntity)
			end
		end
	end

	function ENT:StartTouch(ent)
		if self:GetHitTime() ~= 0 or not ent:IsValid() then return end

		local owner = self:GetOwner()

		if ent == owner then return end

		self:SetHitTime(CurTime())

		local phys = self:GetPhysicsObject()
		if phys:IsValid() then
			phys:EnableMotion(false)
			phys:Sleep()
		end

		self:SetMoveType(MOVETYPE_NONE)

		if ent:IsValid() then
			self:SetSolid(SOLID_NONE)
			self:AddEFlags(EFL_SETTING_UP_BONES)
			self.Stuck = ent

			local followed = false
			/*local bonecount = ent:GetBoneCount()
			if bonecount and bonecount > 1 then
				local boneindex = ent:NearestBone(self:GetPos())
				if boneindex and boneindex > 0 then
					self:FollowBone(ent, boneindex)
					self:SetPos(ent:GetBonePositionMatrixed(boneindex))
					followed = true
				end
			end*/ -- buggy in sandbox: errors on jeeps
			if not followed then
				self:SetParent(ent)
			end
		end
	end

	function ENT:Explode()
		if self.Exploded then return end
		self.Exploded = true

		local alt = self:GetDTBool(0)
		local owner = self.PostOwner:IsValid() and self.PostOwner or self:GetOwner()
		local pos = self:GetPos()
		local radius = 192
		util.BlastDamage(self, owner, pos, radius, 125 * self:GetCharge(), DMG_ALWAYSGIB)
	end

	function ENT:OnTakeDamage(dmginfo)
		self:TakePhysicsDamage(dmginfo)
		self:Explode()
	end

	function ENT:PhysicsCollide(data, physobj)
		self.PhysicsData = data
		self:NextThink(CurTime())
	end
end