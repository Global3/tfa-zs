AddCSLuaFile()
ENT.Type = "anim"

function ENT:ShouldNotCollide(ent)
	return ent:IsPlayer()
end

util.PrecacheModel("models/props_junk/sawblade001a.mdl")

if CLIENT then
	local matGlow = Material("sprites/glow04_noz")

	function ENT:Draw()
		render.SetColorModulation(0, 0.608, 1)
		self:DrawModel()
		render.SetColorModulation(1, 1, 1)

		local pos = self:GetPos()

		local emitter = ParticleEmitter(pos)
		emitter:SetNearClip(24, 32)
		local particle
		for i=0, 1 do
			particle = emitter:Add(matGlow, pos)
			particle:SetVelocity(VectorRand() * 5)
			particle:SetDieTime(0.4)
			particle:SetStartAlpha(125)
			particle:SetEndAlpha(0)
			particle:SetStartSize(6)
			particle:SetEndSize(0)
			particle:SetRollDelta(math.Rand(-10, 10))
			particle:SetColor(110, 210, 255)
		end
		emitter:Finish() emitter = nil collectgarbage("step", 64)
	end

	function ENT:Initialize()
		self:SetModelScale(0.3, 0)
		self:DrawShadow(false)
	end
end

if SERVER then
	function ENT:Initialize()
		self:SetModelScale(0.3, 0)
		self:DrawShadow(false)
		self:SetModel("models/props_junk/sawblade001a.mdl")
		self:PhysicsInitSphere(3)
		self:SetMoveType(MOVETYPE_VPHYSICS)
		self:SetSolid(SOLID_VPHYSICS)
		local phys = self:GetPhysicsObject()
			if phys:IsValid() then
				phys:SetMass(1)
				phys:SetBuoyancyRatio(0.0001)
				phys:EnableMotion(true)
				phys:EnableGravity(true)
				phys:EnableDrag(false)
				phys:Wake()
			end
		--self:Fire("kill", "", 0.3)
		timer.Simple(0.4, function()
			if self:IsValid() then
				self:Explode(self:GetPos())
			end
		end)
		self.NextShoot = 0
		self.PostOwner = self:GetOwner()
	end

	function ENT:Think()
		if self.PhysicsData then
			self:Explode(self.PhysicsData.HitPos, self.PhysicsData.HitNormal, self.PhysicsData.HitEntity)
		end
	end

	function ENT:PhysicsCollide(data, physobj)
		self.PhysicsData = data
		self:NextThink(CurTime())
	end

	function ENT:OnRemove()
	end

	function ENT:Explode(hitpos, normal, hitent)
	
		if self.Exploded then return end

		local hitpos = self.PhysicsData and self.PhysicsData.HitPos or self:GetPos()
		local normal = self.PhysicsData and self.PhysicsData.HitNormal or Vector(0, 0, 1)

		local effectdata = EffectData()
			effectdata:SetOrigin(hitpos)
			effectdata:SetNormal(normal)
		util.Effect("explosion_fusordisc", effectdata)

		local owner = self.PostOwner
		if not owner:IsValid() then owner = self end

		local oldvel = self.PhysicsData and self.PhysicsData.OurOldVelocity or self:GetVelocity()

		local backvel = oldvel:GetNormalized()
		local pos = self:GetPos()

		for i = 1, 8 do
			/*timer.Simple(i * 0.05, function()
				if (!IsValid(self)) then return end
				backvel.z = backvel.z + 0.001 * i
				backvel = backvel:GetNormalized()
				--self:FireBulletsLua(pos, -backvel, 1, 1, dmg/2, owner, 0.01, "tracer_fusor", BulletCallback, nil, nil, nil, nil, me)
				print(owner)
				print(-backvel)
				print(pos)
				print(self)
				local bullettbl = { owner, 10.5, 0, 56756, 0, 1, 1, "", "tracer_fusor", backvel, Vector( 1, 1, 1), self:GetPos() - backvel * 10 }
				PrintTable(bullettbl)
				
				self:FireBullets(bullettbl)
			end)*/
			timer.Simple(i * 0.05, function()
				if IsValid(self) then
					util.BlastDamage(self, owner, hitpos, 192, 30)
				end
			end)
		end
		
		self.Exploded = true

		timer.Simple(0.3, function()
			self:Remove()
		end)
	end
end