AddCSLuaFile()
ENT.Type = "anim"

function ENT:ShouldNotCollide(ent)
	return ent:IsPlayer()
end

if SERVER then
	function ENT:Initialize()
		self:SetModel("models/healthvial.mdl")
		self:PhysicsInitSphere(1)
		self:SetSolid(SOLID_VPHYSICS)
		local phys = self:GetPhysicsObject()
			if phys:IsValid() then
				phys:SetMass(1)
				phys:SetBuoyancyRatio(0.0001)
				phys:EnableMotion(true)
				phys:EnableGravity(true)
				phys:EnableDrag(false)
				phys:Wake()
			end
		self:Fire("kill", "", 10)
	end

	function ENT:OnRemove()
		local ent = ents.Create("env_mediccloud")
		if ent:IsValid() then
			ent:SetPos(self.HitPos or self:GetPos())
			ent:SetOwner(self.Owner or self:GetOwner())
			ent:Spawn()
		end
	end

	function ENT:PhysicsCollide(data, phys)
		self.HitPos = self:GetPos() --data.HitPos + data.HitNormal * 8
		self.Owner = self:GetOwner()
		self:Fire("kill", "", 0.01)
	end
end

if CLIENT then
	ENT.RenderGroup = RENDERGROUP_TRANSLUCENT
	local matGlow = Material("sprites/light_glow02_add")
	function ENT:DrawTranslucent()
		self:DrawModel()
		render.SetMaterial(matGlow)
		render.DrawSprite(self:GetPos(), 64, 64, Color(0,255,0,0))
	end
end