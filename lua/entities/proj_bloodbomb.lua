AddCSLuaFile()
ENT.Type = "anim"

ENT.Radius = 75

function ENT:ShouldNotCollide(ent)
	return ent:IsPlayer()
end

if CLIENT then
	function ENT:Draw()
		render.SetColorModulation(0.2, 0.2, 0.2)
		self:DrawModel()
		render.SetColorModulation(1, 1, 1)
	end

	function ENT:OnRemove()
		self:EmitSound("items/suitchargeok1.wav", 75, 80)

		local pos = self:GetPos()
		local emitter = ParticleEmitter(pos)
		emitter:SetNearClip(12, 16)

		for i = 1, 10 do
			local axis = AngleRand()
			for j=1, 40 do
				axis.roll = axis.roll + 8
				offset = axis:Up()

				particle = emitter:Add("sprites/glow04_noz", pos + offset)
				particle:SetVelocity(offset * math.Rand(300, 400))
				particle:SetGravity(Vector(0, 0, -300))
				particle:SetColor(255, 90, 90)
				particle:SetAirResistance(200)
				particle:SetDieTime(math.Rand(1.95, 2.5))
				particle:SetStartAlpha(205)
				particle:SetEndAlpha(0)
				particle:SetStartSize(1)
				particle:SetEndSize(math.Rand(6, 10))
				particle:SetRoll(math.Rand(0, 360))
				particle:SetRollDelta(math.Rand(-10, 10))
				particle:SetCollide(true)
			end

			for j=1, 5 do
				particle = emitter:Add("sprites/glow04_noz", pos)
				particle:SetVelocity(VectorRand() * 8)
				particle:SetColor(255, 60, 60)
				particle:SetDieTime(2)
				particle:SetStartAlpha(0)
				particle:SetEndAlpha(255)
				particle:SetStartSize(24)
				particle:SetEndSize(0)
				particle:SetRoll(math.Rand(0, 360))
			end
		end
		emitter:Finish() emitter = nil collectgarbage("step", 64)
	end
end

if SERVER then
	function ENT:Initialize()
		self:SetModel("models/props_lab/labpart.mdl")
		self:PhysicsInitSphere(2)
		self:SetSolid(SOLID_VPHYSICS)
		self:SetModelScale(0.42, 0)
		self:SetCustomCollisionCheck(true)
		self:SetCollisionGroup(COLLISION_GROUP_PROJECTILE)

		local phys = self:GetPhysicsObject()
		if phys:IsValid() then
			phys:SetMass(1)
			phys:SetBuoyancyRatio(0.01)
			phys:SetDamping(1.5, 4)
			phys:EnableDrag(false)
			phys:Wake()
		end
	end

	function ENT:Think()
		if self.PhysicsData then
			self:Explode(self.PhysicsData.HitPos, self.PhysicsData.HitNormal)
		end
	end

	function ENT:PhysicsCollide(data, phys)
		self.PhysicsData = data
		self:NextThink(CurTime())
	end

	function ENT:Explode(hitpos, hitnormal)
		if self.Exploded then return end
		self.Exploded = true

		self:Fire("kill", "", 0.01)

		local owner = self:GetOwner()

		hitpos = hitpos or self:GetPos()
		if not hitnormal then
			hitnormal = self:GetVelocity():GetNormalized() * -1
		end

		if owner:IsPlayer() then
			for _, ent in pairs(ents.FindInSphere(hitpos, self.Radius)) do
				if ent and ent:IsPlayer() then
					for i = 1, 50 do
						timer.Simple(i * 0.02, function()
							if IsValid(ent) then
								if ent:Armor() < 100 and ent:Alive() == true then
									ent:SetArmor(ent:Armor() + 1)
								end
							end		
						end)
					end
					owner:AddFrags(5)
				end
			end
			self:NextThink(CurTime())
		end
	end
end
