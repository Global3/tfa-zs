if SERVER then
	AddCSLuaFile()
	SWEP.Weight			= 1
	SWEP.AutoSwitchTo	= false
	SWEP.AutoSwitchFrom	= true	
end

if CLIENT then

	SWEP.PrintName     	    = "Bloodshot Bomb"			
	SWEP.Slot				= 1
	SWEP.SlotPos			= 1
	SWEP.DrawAmmo			= false
	SWEP.DrawCrosshair		= true

end

SWEP.Author			= "Zet0r, Global"
SWEP.Contact		= ""
SWEP.Purpose		= "Throws a grenade if you have any"
SWEP.Instructions	= "Let the gamemode give you it"

SWEP.Spawnable			= false
SWEP.AdminSpawnable		= false

SWEP.HoldType = "grenade"

SWEP.ViewModel = "models/weapons/c_grenade.mdl"
SWEP.WorldModel	= "models/weapons/w_grenade.mdl"
SWEP.UseHands = true
SWEP.vModel = true

SWEP.Primary.ClipSize		= -1
SWEP.Primary.DefaultClip	= -1
SWEP.Primary.Automatic		= false
SWEP.Primary.Ammo			= "none"

SWEP.Secondary.ClipSize		= -1
SWEP.Secondary.DefaultClip	= -1
SWEP.Secondary.Automatic	= false
SWEP.Secondary.Ammo			= "none"

SWEP.NextReload				= 1

SWEP.MaterialTable = {
    [1] = "nodraw",
	[2] = "nodraw",
	[3] = "nodraw"
}

SWEP.VElements = {
	["corrosive_nade++"] = { type = "Model", model = "models/healthvial.mdl", bone = "ValveBiped.Grenade_body", rel = "corrosive_nade", pos = Vector(0, -7, 0.4), angle = Angle(0, 0, 90), size = Vector(0.75, 0.75, 1.299), color = Color(255, 59, 0, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
	["corrosive_nade+"] = { type = "Model", model = "models/props_wasteland/laundry_washer003.mdl", bone = "ValveBiped.Grenade_body", rel = "corrosive_nade", pos = Vector(0, -0.201, 0.4), angle = Angle(0, 90, 0), size = Vector(0.119, 0.019, 0.019), color = Color(100, 100, 100, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
	["corrosive_nade"] = { type = "Model", model = "models/props_lab/labpart.mdl", bone = "ValveBiped.Grenade_body", rel = "", pos = Vector(0, 0, -1), angle = Angle(0, 0, 90), size = Vector(0.449, 0.55, 0.5), color = Color(200, 200, 200, 255), surpresslightning = false, material = "models/weapons/v_smg1/v_smg1_sheet", skin = 0, bodygroup = {} }
}

SWEP.WElements = {
	["corrosive_nade+"] = { type = "Model", model = "models/props_wasteland/laundry_washer003.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "corrosive_nade", pos = Vector(0, -0.201, 0.4), angle = Angle(0, 90, 0), size = Vector(0.119, 0.019, 0.019), color = Color(100, 100, 100, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
	["corrosive_nade++"] = { type = "Model", model = "models/healthvial.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "corrosive_nade", pos = Vector(0, -7, 0.4), angle = Angle(0, 0, 90), size = Vector(0.75, 0.75, 1.299), color = Color(255, 59, 0, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
	["corrosive_nade"] = { type = "Model", model = "models/props_lab/labpart.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "", pos = Vector(3.5, 2, 0), angle = Angle(0, 0, 99.35), size = Vector(0.449, 0.55, 0.5), color = Color(200, 200, 200, 255), surpresslightning = false, material = "models/weapons/v_smg1/v_smg1_sheet", skin = 0, bodygroup = {} }
}

function SWEP:Initialize()

	self:SetHoldType( self.HoldType )

end

function SWEP:Deploy()
	self:SendWeaponAnim(ACT_VM_DRAW)
	--if !self.Owner:GetUsingSpecialWeapon() then
		--self.Owner:EquipPreviousWeapon()
	--end
end

function SWEP:PrimaryAttack()
	self:ThrowGrenade(1000)
end

function SWEP:ThrowGrenade(force)
	self.Owner:SetAnimation(PLAYER_ATTACK1)
	self:SendWeaponAnim(ACT_VM_THROW)
	
	if SERVER then
		local nade = ents.Create("nz_bloodnade_ent")
		nade:SetPos(self.Owner:EyePos() + (self.Owner:GetAimVector() * 20))
		nade:SetAngles( Angle(30,0,0)  )
		nade:Spawn()
		nade:Activate()
		nade:SetOwner(self.Owner)
		
		local nadePhys = nade:GetPhysicsObject()
			if !IsValid(nadePhys) then return end
		nadePhys:ApplyForceCenter(self.Owner:GetAimVector():GetNormalized() * force + self.Owner:GetVelocity())
		nadePhys:AddAngleVelocity(Vector(1000,0,0))	
	end
end

function SWEP:PostDrawViewModel()

end

function SWEP:DrawWorldModel()
end

function SWEP:OnRemove()
	
end

function SWEP:Holster( wep )
	--if not IsFirstTimePredicted() then return end
	return true
end

function SWEP:Equip( NewOwner )
	NewOwner:ChatPrint("You've obtained a bloodshot bomb! Your normal grenade has been replaced.")
	NewOwner:ChatPrint("This gives all teammates within the explosion 100 armor.")
	NewOwner:ChatPrint("Caps at 300 armor. Doesn't effect zombies!")
end