--General--
SWEP.PrintName		= "'Jackhammer' Drum Shotgun"
SWEP.Purpose		= "An automatic drum shotgun with a large clip size."
SWEP.Slot			= 3
SWEP.SlotPos		= 0
SWEP.Category		= "TFA ZS"
SWEP.Author			= "Global, JetBoom, Raox"
SWEP.Spawnable		= true
SWEP.AdminSpawnable	= true

sound.Add(
{
	name = "Weapon_JackHammer.Single",
	channel = CHAN_WEAPON,
	volume = 0.7,
	soundlevel = 100,
	pitch = {147,153},
	sound = {"weapons/xm1014/xm1014-1.wav"}
})

sound.Add(
{
	name = "Weapon_JackHammer.Single2",
	channel = CHAN_WEAPON+20,
	volume = 0.7,
	soundlevel = 100,
	pitch = {132,138},
	sound = {"weapons/shotgun/shotgun_fire6.wav"}
})

SWEP.Base = "tfa_gun_base"

--Model and Animations--
SWEP.HoldType = "shotgun"
SWEP.ViewModel = "models/weapons/cstrike/c_rif_galil.mdl"
SWEP.WorldModel = "models/weapons/w_rif_galil.mdl"
SWEP.UseHands = true
SWEP.ViewModelFOV = 59
SWEP.ViewModelFlip = false
SWEP.ShowViewModel = true
SWEP.ShowWorldModel = false
SWEP.DisableIdleAnimations	= true
SWEP.ForceDryFireOff		= true
SWEP.DoMuzzleFlash			= true
SWEP.MuzzleAttachment		= "1"
SWEP.MuzzleFlashEffect		= nil
SWEP.SequenceEnabled = { /*[ACT_VM_NAME] = boolean*/ }
SWEP.MaterialTable = {
    [1] = "nodraw",
	[2] = "nodraw",
	[3] = "nodraw"
}
SWEP.SequenceRateOverride = {
    [ACT_VM_RELOAD] = 0.65
}

--Sight Positions--
SWEP.data						= {}
SWEP.data.ironsights			= 0
SWEP.Secondary.IronFOV			= 65

SWEP.IronSightsPos = Vector(-5.75, 10, 2.7)
SWEP.IronSightsAng = Vector(0, 0, 0)

SWEP.RunSightsPos = Vector(4, -1, 0)
SWEP.RunSightsAng = Vector(-12, 25, 0)

--Projectiles--
SWEP.ProjectileEntity			= nil
SWEP.ProjectileVelocity			= 1500
SWEP.ProjectileModel			= nil

SWEP.NZPaPName			= "Bulldozer"	-- What name this weapon should use when Pack-a-Punched.

function SWEP:OnPaP()
self.Primary.Damage = self.Primary.Damage*2.5
self.Primary.ClipSize = 12
self.Primary.ClipSize_Orig = 12
self.Primary.ClipSize_ORIG_REAL = 12
self.pap = true
end

--Gun Behavior---
SWEP.Primary.Sound				= Sound("Weapon_JackHammer.Single")
SWEP.Primary.Ammo				= "buckshot"
SWEP.Primary.Damage				= 10.5 -- only shown in C menu
SWEP.Primary.NumShots			= 8
SWEP.Primary.AmmoConsumption	= 1
SWEP.Primary.ClipSize			= 12
SWEP.Primary.DefaultClip		= 108
SWEP.Primary.Automatic			= true
SWEP.DisableChambering			= false
SWEP.Primary.RPM				= 235

--Accuracy--
SWEP.Primary.KickUp					= 0.4
SWEP.Primary.KickDown				= 0.2
SWEP.Primary.KickHorizontal			= 0.5
SWEP.Primary.StaticRecoilFactor		= 0.3
SWEP.Primary.SpreadMultiplierMax	= 4
SWEP.Primary.SpreadIncrement		= 0.18
SWEP.Primary.SpreadRecovery			= 2
SWEP.Primary.Spread					= 0.075
SWEP.Primary.IronAccuracy			= 0.06

--Effects--
SWEP.TracerName					= nil
SWEP.TracerCount				= 1
SWEP.EventTable = {
	[ACT_VM_PRIMARYATTACK] = {
		{ ["time"] = 0, ["type"] = "sound", ["value"] = Sound("Weapon_JackHammer.Single2") }
	},
	[ACT_VM_RELOAD] = {
		{ ["time"] = 0, ["type"] = "sound", ["value"] = Sound("Weapon_Deagle.Clipout") }
	}
}
util.PrecacheSound("weapons/xm1014/xm1014-1.wav")
util.PrecacheSound("weapons/shotgun/shotgun_fire6.wav")

--Model Elements--
SWEP.VElements = {
	["t4_shot_part+++++++"] = { type = "Model", model = Model("models/props_pipes/concrete_pipe001a.mdl"), bone = "ValveBiped.Bip01_R_Hand", rel = "t4_shot_part+", pos = Vector(0, 11, 1.5), angle = Angle(90, 0, 0), size = Vector(0.032, 0.009, 0.009), color = Color(30, 30, 30, 255), surpresslightning = false, material = "models/props_pipes/pipemetal001a", skin = 0, bodygroup = {} },
	["t4_shot_part++++++"] = { type = "Model", model = Model("models/props_pipes/pipe02_straight01_long.mdl"), bone = "ValveBiped.Bip01_R_Hand", rel = "t4_shot_part+", pos = Vector(0, 13, -0.801), angle = Angle(0, 0, 0), size = Vector(0.17, 0.55, 0.17), color = Color(40, 40, 40, 255), surpresslightning = false, material = "models/props_pipes/pipemetal001a", skin = 0, bodygroup = {} },
	["t4_shot_part++++"] = { type = "Model", model = Model("models/props_wasteland/controlroom_filecabinet002a.mdl"), bone = "ValveBiped.Bip01_R_Hand", rel = "t4_shot_part+", pos = Vector(0, -13, 0.618), angle = Angle(0, 0, 100), size = Vector(0.039, 0.15, 0.009), color = Color(60, 60, 60, 255), surpresslightning = false, material = "phoenix_storms/metalfloor_2-3", skin = 0, bodygroup = {} },
	["t4_shot_part+++"] = { type = "Model", model = Model("models/props_junk/ibeam01a_cluster01.mdl"), bone = "ValveBiped.Bip01_R_Hand", rel = "t4_shot_part+", pos = Vector(0, -7.301, 0.66), angle = Angle(180, -90, 0), size = Vector(0.059, 0.029, 0.039), color = Color(47, 22, 1, 255), surpresslightning = false, material = "models/props_pipes/pipemetal004a", skin = 0, bodygroup = {} },
	["t4_shot_part++"] = { type = "Model", model = Model("models/props_combine/combine_interface003.mdl"), bone = "ValveBiped.Bip01_R_Hand", rel = "t4_shot_part+", pos = Vector(0, -6.301, 2.799), angle = Angle(180, -90, 0), size = Vector(0.059, 0.022, 0.059), color = Color(79, 100, 135, 255), surpresslightning = false, material = "models/props_pipes/pipeset_metal", skin = 0, bodygroup = {} },
	["t4_shot_part+++++"] = { type = "Model", model = Model("models/props_wasteland/laundry_washer001a.mdl"), bone = "v_weapon.magazine", rel = "", pos = Vector(0, -0.801, 3), angle = Angle(0, 0, 0), size = Vector(0.054, 0.054, 0.07), color = Color(50, 50, 50, 255), surpresslightning = false, material = "models/props_canal/canal_bridge_railing_01c", skin = 0, bodygroup = {} },
	["t4_shot_part+"] = { type = "Model", model = Model("models/props_combine/combine_train02a.mdl"), bone = "ValveBiped.Bip01_R_Hand", rel = "t4_shot_part", pos = Vector(24.5, 4.38, -4.1), angle = Angle(180, 90, 0), size = Vector(0.013, 0.024, 0.009), color = Color(60, 60, 60, 255), surpresslightning = false, material = "models/props_pipes/pipemetal001a", skin = 0, bodygroup = {} },
	["t4_shot_part"] = { type = "Model", model = Model("models/weapons/c_pistol.mdl"), bone = "v_weapon.galil", rel = "", pos = Vector(4.4, -5, -19.8), angle = Angle(90, 0, -90), size = Vector(0.8, 0.8, 1.21), color = Color(60, 60, 60, 255), surpresslightning = false, material = "models/props_pipes/pipemetal001a", skin = 0, bodygroup = {} }
}

SWEP.WElements = {
	["t4_shot_part++++++"] = { type = "Model", model = Model("models/props_pipes/pipe02_straight01_long.mdl"), bone = "ValveBiped.Bip01_R_Hand", rel = "t4_shot_part+", pos = Vector(0, 13, -0.801), angle = Angle(0, 0, 0), size = Vector(0.17, 0.3, 0.17), color = Color(30, 30, 30, 255), surpresslightning = false, material = "models/props_pipes/pipemetal001a", skin = 0, bodygroup = {} },
	["t4_shot_part+++++++"] = { type = "Model", model = Model("models/props_pipes/concrete_pipe001a.mdl"), bone = "ValveBiped.Bip01_R_Hand", rel = "t4_shot_part+", pos = Vector(0, 11, 1.5), angle = Angle(90, 0, 0), size = Vector(0.032, 0.009, 0.009), color = Color(30, 30, 30, 255), surpresslightning = false, material = "models/props_pipes/pipemetal001a", skin = 0, bodygroup = {} },
	["t4_shot_part++++"] = { type = "Model", model = Model("models/props_wasteland/controlroom_filecabinet002a.mdl"), bone = "ValveBiped.Bip01_R_Hand", rel = "t4_shot_part+", pos = Vector(0, -13, 0.618), angle = Angle(0, 0, 100), size = Vector(0.039, 0.15, 0.009), color = Color(60, 60, 60, 255), surpresslightning = false, material = "phoenix_storms/metalfloor_2-3", skin = 0, bodygroup = {} },
	["t4_shot_part+++"] = { type = "Model", model = Model("models/props_junk/ibeam01a_cluster01.mdl"), bone = "ValveBiped.Bip01_R_Hand", rel = "t4_shot_part+", pos = Vector(0, -7.301, 0.66), angle = Angle(180, -90, 0), size = Vector(0.059, 0.029, 0.039), color = Color(47, 22, 1, 255), surpresslightning = false, material = "models/props_pipes/pipemetal004a", skin = 0, bodygroup = {} },
	["t4_shot_part++"] = { type = "Model", model = Model("models/props_combine/combine_interface003.mdl"), bone = "ValveBiped.Bip01_R_Hand", rel = "t4_shot_part+", pos = Vector(0, -6.301, 2.799), angle = Angle(180, -90, 0), size = Vector(0.059, 0.029, 0.059), color = Color(79, 100, 135, 255), surpresslightning = false, material = "models/props_pipes/pipeset_metal", skin = 0, bodygroup = {} },
	["t4_shot_part+++++"] = { type = "Model", model = Model("models/props_wasteland/laundry_washer001a.mdl"), bone = "ValveBiped.Bip01_R_Hand", rel = "t4_shot_part+", pos = Vector(0, 5, 2), angle = Angle(0, 0, 90), size = Vector(0.054, 0.054, 0.07), color = Color(50, 50, 50, 255), surpresslightning = false, material = "models/props_canal/canal_bridge_railing_01c", skin = 0, bodygroup = {} },
	["t4_shot_part+"] = { type = "Model", model = Model("models/props_combine/combine_train02a.mdl"), bone = "ValveBiped.Bip01_R_Hand", rel = "t4_shot_part", pos = Vector(-3.636, 0, 0.699), angle = Angle(0, 90, 180), size = Vector(0.013, 0.024, 0.009), color = Color(50, 50, 50, 255), surpresslightning = false, material = "models/props_pipes/pipemetal001a", skin = 0, bodygroup = {} },
	["t4_shot_part"] = { type = "Model", model = Model("models/weapons/w_pistol.mdl"), bone = "ValveBiped.Bip01_R_Hand", rel = "", pos = Vector(3, 1, -4.5), angle = Angle(0, 180, 180), size = Vector(0.8, 0.8, 1.21), color = Color(50, 50, 50, 255), surpresslightning = false, material = "models/props_pipes/pipemetal001a", skin = 0, bodygroup = {} }
}

SWEP.ViewModelBoneMods = {
	["ValveBiped.Bip01_L_Hand"] = { scale = Vector(1, 1, 1), pos = Vector(0, 0, 0), angle = Angle(0, 0, 21) }
}