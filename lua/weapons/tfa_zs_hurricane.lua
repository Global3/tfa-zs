--General--
SWEP.PrintName		= "'Hurricane' Pulse SMG"
SWEP.Purpose		= "Fires rapid pulse shots that slow targets."
SWEP.Slot			= 2
SWEP.SlotPos		= 0
SWEP.Category		= "TFA ZS"
SWEP.Author			= "Global, JetBoom, Raox"
SWEP.Spawnable		= true
SWEP.AdminSpawnable	= true

sound.Add(
{
	name = "Weapon_Hurricane.Single",
	channel = CHAN_WEAPON,
	volume = 0.7,
	soundlevel = 100,
	pitch = {70,80},
	sound = {"weapons/ar2/fire1.wav"}
})

SWEP.Base = "tfa_gun_base"

--Model and Animations--
SWEP.HoldType = "smg"
SWEP.ViewModel = "models/weapons/c_smg1.mdl"
SWEP.WorldModel = "models/weapons/w_smg1.mdl"
SWEP.UseHands = true
SWEP.ViewModelFOV = 60
SWEP.ViewModelFlip = false
SWEP.ShowViewModel = true
SWEP.ShowWorldModel = true
SWEP.DisableIdleAnimations	= true
SWEP.ForceDryFireOff		= true
SWEP.DoMuzzleFlash			= true
SWEP.MuzzleAttachment		= "muzzle"
SWEP.MuzzleFlashEffect		= "tfa_muzzleflash_cryo"
SWEP.SequenceEnabled = { /*[ACT_VM_NAME] = boolean*/ }
SWEP.MaterialTable = {
    --[1] = "nodraw",
    --[2] = "nodraw",
	--[3] = "nodraw"
}

--Sight Positions--
SWEP.data						= {}
SWEP.data.ironsights			= 1
SWEP.Secondary.IronFOV			= 55

SWEP.IronSightsPos = Vector(-6.425, 5, 1.02)
SWEP.IronSightsAng = Vector(0, 0, 0)

SWEP.RunSightsPos = Vector(4, -1, 0)
SWEP.RunSightsAng = Vector(-12, 25, 0)

--Projectiles--
SWEP.ProjectileEntity			= nil
SWEP.ProjectileVelocity			= 1500
SWEP.ProjectileModel			= nil

SWEP.NZPaPName			= "Category 5"	-- What name this weapon should use when Pack-a-Punched.

function SWEP:OnPaP()
self.Primary.Damage = self.Primary.Damage*2.5
self.Primary.ClipSize = 40
self.Primary.ClipSize_Orig = 40
self.Primary.ClipSize_ORIG_REAL = 40
self.pap = true
end

--Gun Behavior---
SWEP.Primary.Sound				= Sound("Weapon_Hurricane.Single")
SWEP.Primary.Ammo				= "SMG1"
SWEP.Primary.Damage				= 14.5 -- only shown in C menu
SWEP.Primary.AmmoConsumption	= 1
SWEP.Primary.ClipSize			= 40
SWEP.Primary.DefaultClip		= 280
SWEP.Primary.Automatic			= true
SWEP.DisableChambering			= true
SWEP.Primary.RPM				= 800

--Accuracy--
SWEP.Primary.KickUp					= 0.2
SWEP.Primary.KickDown				= 0.1
SWEP.Primary.KickHorizontal			= 0.25
SWEP.Primary.StaticRecoilFactor		= 0.1
SWEP.Primary.SpreadMultiplierMax	= 4
SWEP.Primary.SpreadIncrement		= 0.18
SWEP.Primary.SpreadRecovery			= 2
SWEP.Primary.Spread					= 0.035
SWEP.Primary.IronAccuracy			= 0.04

--Effects--
SWEP.TracerName					= "AR2Tracer"
SWEP.TracerCount				= 1
SWEP.EventTable = {
	[ACT_VM_RELOAD] = {
		{ ["time"] = 0, ["type"] = "sound", ["value"] = Sound("weapons/smg1/smg1_reload.wav") }
	}
}
util.PrecacheSound("weapons/smg1/smg1_reload.wav")

--Model Elements--
SWEP.VElements = {
	["base"] = { type = "Model", model = "models/props_combine/combine_train02a.mdl", bone = "ValveBiped.base", rel = "", pos = Vector(0, -0.301, 4.731), angle = Angle(0, 180, 90), size = Vector(0.025, 0.025, 0.014), color = Color(255, 228, 0, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
	["base+++"] = { type = "Model", model = "models/props_c17/light_domelight01_off.mdl", bone = "ValveBiped.base", rel = "", pos = Vector(0.559, -1.04, -3.651), angle = Angle(-90, -164.752, 0), size = Vector(0.043, 0.037, 0.052), color = Color(255, 255, 255, 255), surpresslightning = true, material = "models/error/new light1", skin = 0, bodygroup = {} },
	["base+"] = { type = "Model", model = "models/props_combine/eli_pod_inner.mdl", bone = "ValveBiped.base", rel = "", pos = Vector(0.079, -2.368, -6.355), angle = Angle(-0.703, -90.113, 0), size = Vector(0.12, 0.075, 0.116), color = Color(255, 204, 0, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
	["base++"] = { type = "Model", model = "models/props_combine/combine_teleportplatform.mdl", bone = "ValveBiped.base", rel = "", pos = Vector(-0.002, -0.489, -8.968), angle = Angle(0, -90, -0), size = Vector(0.056, 0.019, 0.052), color = Color(255, 209, 0, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} }
}

SWEP.WElements = {
	["base+++"] = { type = "Model", model = "models/props_c17/light_domelight01_off.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "", pos = Vector(2.773, 2.071, -5.448), angle = Angle(0, 0, -80.556), size = Vector(0.043, 0.037, 0.052), color = Color(255, 255, 255, 255), surpresslightning = true, material = "models/error/new light1", skin = 0, bodygroup = {} },
	["base++++"] = { type = "Model", model = "models/props_c17/light_domelight01_off.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "", pos = Vector(2.69, 0.799, -5.528), angle = Angle(0, 0, 80.789), size = Vector(0.043, 0.037, 0.052), color = Color(255, 255, 255, 255), surpresslightning = true, material = "models/error/new light1", skin = 0, bodygroup = {} },
	["base++"] = { type = "Model", model = "models/props_combine/combine_teleportplatform.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "", pos = Vector(-1.594, 1.468, -3.698), angle = Angle(103.306, 180, 0), size = Vector(0.056, 0.019, 0.052), color = Color(255, 209, 0, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
	["base"] = { type = "Model", model = "models/props_combine/combine_train02a.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "", pos = Vector(10.47, 1.462, -6.394), angle = Angle(-175.984, -88.995, 11.291), size = Vector(0.025, 0.019, 0.014), color = Color(255, 228, 0, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
	["base+"] = { type = "Model", model = "models/props_combine/eli_pod_inner.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "", pos = Vector(-0.541, 1.491, -6.099), angle = Angle(98.305, 179.197, 0), size = Vector(0.12, 0.075, 0.116), color = Color(255, 204, 0, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} }
}