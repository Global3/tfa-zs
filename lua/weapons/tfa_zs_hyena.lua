--General--
SWEP.PrintName		= "'Hyena' Sticky Bomb Launcher"
SWEP.Purpose		= "Fires explosives that will stick to surfaces and enemies until detonated. Bombs take 3 seconds to reach maximum damage. Alt fire will remotely detonate bombs."
SWEP.Slot			= 4
SWEP.SlotPos		= 0
SWEP.Category		= "TFA ZS"
SWEP.Author			= "Global, JetBoom, Raox"
SWEP.Spawnable		= true
SWEP.AdminSpawnable	= true

sound.Add(
{
	name = "Weapon_Hyena.Single",
	channel = CHAN_WEAPON+10,
	volume = 0.7,
	soundlevel = 100,
	pitch = {112,120},
	sound = {"weapons/ar2/ar2_altfire.wav"}
})

SWEP.Base = "tfa_gun_base"

--Model and Animations--
SWEP.HoldType = "smg"
SWEP.ViewModel = "models/weapons/cstrike/c_smg_p90.mdl"
SWEP.WorldModel = "models/weapons/w_smg_p90.mdl"
SWEP.UseHands = true
SWEP.ViewModelFOV = 50
SWEP.ViewModelFlip = false
SWEP.ShowViewModel = true
SWEP.ShowWorldModel = false
SWEP.DisableIdleAnimations	= true
SWEP.ForceDryFireOff		= true
SWEP.DoMuzzleFlash			= true
SWEP.MuzzleAttachment		= "1"
SWEP.MuzzleFlashEffect		= nil
SWEP.SequenceEnabled = { /*[ACT_VM_NAME] = boolean*/ }
SWEP.MaterialTable = {
    [1] = "nodraw",
    [2] = "nodraw",
	[3] = "nodraw"
}

--Sight Positions--
SWEP.data						= {}
SWEP.data.ironsights			= 0
SWEP.Secondary.IronFOV			= 55

SWEP.IronSightsPos = Vector(-6.425, 5, 1.02)
SWEP.IronSightsAng = Vector(0, 0, 0)

SWEP.RunSightsPos = Vector(4, -1, 0)
SWEP.RunSightsAng = Vector(-12, 25, 0)

--Projectiles--
SWEP.ProjectileEntity			= "proj_bomb_sticky"
SWEP.ProjectileVelocity			= 850
SWEP.ProjectileModel			= nil

SWEP.NZPaPName			= "Reactor 4"	-- What name this weapon should use when Pack-a-Punched.

function SWEP:OnPaP()
self.Primary.Damage = self.Primary.Damage*2.5
self.Primary.ClipSize = 3
self.Primary.ClipSize_Orig = 3
self.Primary.ClipSize_ORIG_REAL = 3
self.pap = true
end

--Gun Behavior---
SWEP.Primary.Sound				= Sound("Weapon_Hyena.Single")
SWEP.Primary.Ammo				= "slam"
SWEP.Primary.Damage				= 125 -- only shown in C menu
SWEP.Primary.AmmoConsumption	= 1
SWEP.Primary.ClipSize			= 3
SWEP.Primary.DefaultClip		= 9
SWEP.Primary.Automatic			= true
SWEP.DisableChambering			= true
SWEP.Primary.RPM				= 300

--Accuracy--
SWEP.Primary.KickUp					= 0.2
SWEP.Primary.KickDown				= 0.1
SWEP.Primary.KickHorizontal			= 0.25
SWEP.Primary.StaticRecoilFactor		= 0.1
SWEP.Primary.SpreadMultiplierMax	= 4
SWEP.Primary.SpreadIncrement		= 0.18
SWEP.Primary.SpreadRecovery			= 2
SWEP.Primary.Spread					= 0.001
SWEP.Primary.IronAccuracy			= 0.001

--Effects--
SWEP.TracerName					= nil
SWEP.TracerCount				= 1
SWEP.EventTable = {
	[ACT_VM_PRIMARYATTACK] = {
		{ ["time"] = 0, ["type"] = "sound", ["value"] = Sound("weapons/physcannon/superphys_launch1.wav") }
	}
}

util.PrecacheSound("weapons/ar2/ar2_altfire.wav")
util.PrecacheSound("weapons/physcannon/superphys_launch1.wav")

--Model Elements--
SWEP.VElements = {
	["base+++"] = { type = "Model", model = "models/props_combine/combine_interface002.mdl", bone = "v_weapon.p90_Parent", rel = "base", pos = Vector(-3.22, -0.101, 4.764), angle = Angle(180, 0, 180), size = Vector(0.048, 0.021, 0.07), color = Color(20, 24, 24, 255), surpresslightning = false, material = "models/props_pipes/valve001_skin1", skin = 0, bodygroup = {} },
	["base++"] = { type = "Model", model = "models/props_wasteland/laundry_basket001.mdl", bone = "v_weapon.p90_Parent", rel = "base", pos = Vector(0.335, 0, -4.156), angle = Angle(180, 90, 0), size = Vector(0.045, 0.045, 0.103), color = Color(52, 52, 64, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
	["base+++++++"] = { type = "Model", model = "models/props_combine/headcrabcannister01a.mdl", bone = "v_weapon.p90_Parent", rel = "base", pos = Vector(0.669, -0.02, 3.68), angle = Angle(-92.179, 0, 0), size = Vector(0.059, 0.013, 0.045), color = Color(19, 22, 21, 255), surpresslightning = false, material = "models/props_pipes/valve001_skin1", skin = 0, bodygroup = {} },
	["base+"] = { type = "Model", model = "models/props_lab/teleplatform.mdl", bone = "v_weapon.p90_Parent", rel = "base", pos = Vector(-0.26, 0, -1.509), angle = Angle(0, 0, 0), size = Vector(0.05, 0.029, 0.103), color = Color(33, 33, 33, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
	["base++++"] = { type = "Model", model = "models/props_combine/combine_booth_short01a.mdl", bone = "v_weapon.p90_Parent", rel = "base", pos = Vector(-0.294, -0.101, 5.506), angle = Angle(0, -180, -180), size = Vector(0.056, 0.014, 0.045), color = Color(24, 27, 27, 255), surpresslightning = false, material = "models/props_pipes/valve001_skin1", skin = 0, bodygroup = {} },
	["base++++++"] = { type = "Model", model = "models/items/combine_rifle_cartridge01.mdl", bone = "v_weapon.p90_Parent", rel = "base", pos = Vector(1.248, 0, 5.406), angle = Angle(-61.563, 0, 0), size = Vector(0.389, 0.501, 0.593), color = Color(19, 20, 19, 255), surpresslightning = false, material = "models/props_pipes/valve001_skin1", skin = 0, bodygroup = {} },
	["base"] = { type = "Model", model = "models/props_wasteland/laundry_washer001a.mdl", bone = "v_weapon.p90_Parent", rel = "", pos = Vector(0, -4.014, -8.922), angle = Angle(0, -90, 0), size = Vector(0.045, 0.014, 0.059), color = Color(54, 74, 78, 255), surpresslightning = false, material = "models/props_pipes/guttermetal01a", skin = 0, bodygroup = {} },
	["base+++++"] = { type = "Model", model = "models/props_lab/reciever01a.mdl", bone = "v_weapon.p90_Parent", rel = "base", pos = Vector(0.024, 0, 7.936), angle = Angle(0, -90, -90), size = Vector(0.134, 0.547, 0.184), color = Color(24, 27, 27, 255), surpresslightning = false, material = "models/props_pipes/valve001_skin1", skin = 0, bodygroup = {} },
	["clipbase"] = { type = "Model", model = "models/props_c17/light_decklight01_off.mdl", bone = "v_weapon.p90_Clip", rel = "", pos = Vector(0.119, -0.375, -4.686), angle = Angle(0.279, -90, -90), size = Vector(0.157, 0.495, 0.074), color = Color(45, 30, 30, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
	["base++++++++"] = { type = "Model", model = "models/props_wasteland/prison_padlock001a.mdl", bone = "v_weapon.p90_Parent", rel = "base", pos = Vector(1.782, 0, 13.51), angle = Angle(-180, 90, 0), size = Vector(0.703, 0.745, 1.062), color = Color(37, 40, 41, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} }
}

SWEP.WElements = {
	["base+++++"] = { type = "Model", model = "models/props_lab/reciever01a.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "base", pos = Vector(0.024, 0, 7.936), angle = Angle(0, -90, -90), size = Vector(0.134, 0.547, 0.184), color = Color(24, 27, 27, 255), surpresslightning = false, material = "models/props_pipes/valve001_skin1", skin = 0, bodygroup = {} },
	["base++"] = { type = "Model", model = "models/props_wasteland/laundry_basket001.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "base", pos = Vector(0.335, 0, -4.156), angle = Angle(180, 90, 0), size = Vector(0.045, 0.045, 0.103), color = Color(52, 52, 64, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
	["base+++++++"] = { type = "Model", model = "models/props_combine/headcrabcannister01a.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "base", pos = Vector(0.669, -0.02, 3.68), angle = Angle(-92.179, 0, 0), size = Vector(0.059, 0.013, 0.045), color = Color(19, 22, 21, 255), surpresslightning = false, material = "models/props_pipes/valve001_skin1", skin = 0, bodygroup = {} },
	["base+"] = { type = "Model", model = "models/props_lab/teleplatform.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "base", pos = Vector(-0.26, 0, -1.509), angle = Angle(0, 0, 0), size = Vector(0.05, 0.029, 0.103), color = Color(33, 33, 33, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
	["base++++"] = { type = "Model", model = "models/props_combine/combine_booth_short01a.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "base", pos = Vector(-0.294, -0.101, 5.506), angle = Angle(0, -180, -180), size = Vector(0.056, 0.014, 0.045), color = Color(24, 27, 27, 255), surpresslightning = false, material = "models/props_pipes/valve001_skin1", skin = 0, bodygroup = {} },
	["base++++++"] = { type = "Model", model = "models/items/combine_rifle_cartridge01.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "base", pos = Vector(1.248, 0, 5.406), angle = Angle(-61.563, 0, 0), size = Vector(0.389, 0.501, 0.593), color = Color(19, 20, 19, 255), surpresslightning = false, material = "models/props_pipes/valve001_skin1", skin = 0, bodygroup = {} },
	["base"] = { type = "Model", model = "models/props_wasteland/laundry_washer001a.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "", pos = Vector(12.843, 0.741, -5.005), angle = Angle(80.433, 0, 0), size = Vector(0.045, 0.014, 0.059), color = Color(54, 74, 78, 255), surpresslightning = false, material = "models/props_pipes/guttermetal01a", skin = 0, bodygroup = {} },
	["base++++++++"] = { type = "Model", model = "models/props_wasteland/prison_padlock001a.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "base", pos = Vector(1.782, 0, 13.51), angle = Angle(-180, 90, 0), size = Vector(0.703, 0.745, 1.062), color = Color(37, 40, 41, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
	["clipbase"] = { type = "Model", model = "models/props_c17/light_decklight01_off.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "base", pos = Vector(-1.352, -0.051, 7.768), angle = Angle(0, 0, 90), size = Vector(0.157, 0.495, 0.074), color = Color(45, 30, 30, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
	["base+++"] = { type = "Model", model = "models/props_combine/combine_interface002.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "base", pos = Vector(-3.22, -0.101, 4.764), angle = Angle(180, 0, 180), size = Vector(0.048, 0.021, 0.07), color = Color(20, 24, 24, 255), surpresslightning = false, material = "models/props_pipes/valve001_skin1", skin = 0, bodygroup = {} }
}

-- Specialized Coding --
SWEP.MaxBombs	= 3

/*DEFINE_BASECLASS(SWEP.Base)
function SWEP:CanPrimaryAttack( ... )
	if self:Clip1() != 0 and self:GetOwner():GetAmmoCount("slam") != 0 then
		local c = 0
			for _, ent in pairs(ents.FindByClass("proj_bomb_sticky")) do
				if ent:GetOwner() == self:GetOwner() then
					c = c + 1
				end
			end
				if c >= self.MaxBombs then 
					if GetConVar("tfa_zs_hyena_nolimit"):GetInt() == 1 then
					return true
					else
					return false
				end
			end
		return true
	end
end*/ -- uncomment when fixed

function SWEP:SecondaryAttack()
	if self:GetNextSecondaryFire() > CurTime() then return end
	for k,v in pairs(ents.FindByClass("proj_bomb_sticky")) do
		if SERVER then
			if v:GetOwner() == self:GetOwner() then
				v:Explode()
			end
		end
	end
	self:EmitSound("buttons/button9.wav", 100, 100, 0.1)
	self:SetNextSecondaryFire(CurTime() + 0.2)
end