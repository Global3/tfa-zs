--General--
SWEP.PrintName		= "'Gladiator' Super Shotgun"
SWEP.Purpose		= "A pump super shotgun."
SWEP.Slot			= 3
SWEP.SlotPos		= 0
SWEP.Category		= "TFA ZS"
SWEP.Author			= "Global, JetBoom, Raox"
SWEP.Spawnable		= true
SWEP.AdminSpawnable	= true

sound.Add(
{
	name = "Weapon_Gladiator.Single",
	channel = CHAN_WEAPON,
	volume = 0.7,
	soundlevel = 100,
	pitch = {77,100},
	sound = {")weapons/zs_glad/gladshot4.wav"}
})

SWEP.Base = "tfa_gun_base"

--Model and Animations--
SWEP.HoldType = "shotgun"
SWEP.ViewModel = "models/weapons/cstrike/c_shot_m3super90.mdl"
SWEP.WorldModel = "models/weapons/w_shot_m3super90.mdl"
SWEP.UseHands = true
SWEP.ViewModelFOV = 60
SWEP.ViewModelFlip = false
SWEP.ShowViewModel = true
SWEP.ShowWorldModel = false
SWEP.DisableIdleAnimations	= true
SWEP.ForceDryFireOff		= true
SWEP.DoMuzzleFlash			= true
SWEP.MuzzleAttachment		= "1"
SWEP.MuzzleFlashEffect		= nil
SWEP.SequenceEnabled = { /*[ACT_VM_NAME] = boolean*/ }
SWEP.MaterialTable = {
    [1] = "nodraw",
	[2] = "nodraw",
	[3] = "nodraw"
}
SWEP.SequenceRateOverrideScaled = {
    [ACT_VM_PRIMARYATTACK] = 0.8
}
--SWEP.SequenceLengthOverride={
--	[ACT_VM_RELOAD] = 4.5,
--}

--Sight Positions--
SWEP.data						= {}
SWEP.data.ironsights			= 0
SWEP.Secondary.IronFOV			= 65

SWEP.IronSightsPos = Vector(-5.75, 10, 2.7)
SWEP.IronSightsAng = Vector(0, 0, 0)

SWEP.RunSightsPos = Vector(4, -1, 0)
SWEP.RunSightsAng = Vector(-12, 25, 0)

--Projectiles--
SWEP.ProjectileEntity			= nil
SWEP.ProjectileVelocity			= 1500
SWEP.ProjectileModel			= nil

SWEP.NZPaPName			= "The Great Warrior"	-- What name this weapon should use when Pack-a-Punched.

function SWEP:OnPaP()
self.Primary.Damage = self.Primary.Damage*2.5
self.Primary.ClipSize = 12
self.Primary.ClipSize_Orig = 12
self.Primary.ClipSize_ORIG_REAL = 12
self.pap = true
end

--Gun Behavior---
SWEP.Primary.Sound				= Sound("Weapon_Gladiator.Single")
SWEP.Primary.Ammo				= "buckshot"
SWEP.Primary.Damage				= 14
SWEP.Primary.NumShots			= 16
SWEP.Primary.AmmoConsumption	= 2
SWEP.Primary.ClipSize			= 12
SWEP.Primary.DefaultClip		= 108
SWEP.Primary.Automatic			= false
SWEP.DisableChambering			= true
SWEP.Primary.RPM				= 45
SWEP.Shotgun					= true
SWEP.ShellTime					= 0.3

--Accuracy--
SWEP.Primary.KickUp					= 0.6
SWEP.Primary.KickDown				= 0.3
SWEP.Primary.KickHorizontal			= 0.6
SWEP.Primary.StaticRecoilFactor		= 0.5
SWEP.Primary.SpreadMultiplierMax	= 4
SWEP.Primary.SpreadIncrement		= 0.18
SWEP.Primary.SpreadRecovery			= 2
SWEP.Primary.Spread					= 0.06
SWEP.Primary.IronAccuracy			= 0.05

--Effects--
SWEP.TracerName					= nil
SWEP.TracerCount				= 1
--SWEP.EventTable = {
--	[ACT_VM_PRIMARYATTACK] = {
--		{ ["time"] = 0, ["type"] = "sound", ["value"] = Sound("Weapon_JackHammer.Single2") }
--	},
--	[ACT_VM_RELOAD] = {
--		{ ["time"] = 0, ["type"] = "sound", ["value"] = Sound("Weapon_Deagle.Clipout") }
--	}
--}
util.PrecacheSound(")weapons/zs_glad/gladshot4.wav")

--Model Elements--
SWEP.VElements = {
	["fracture+++++++"] = { type = "Model", model = "models/items/combine_rifle_cartridge01.mdl", bone = "v_weapon.M3_PARENT", rel = "fracture", pos = Vector(0, 0, 0.25), angle = Angle(97.013, 0, 0), size = Vector(0.6, 0.4, 1.598), color = Color(77, 100, 135, 255), surpresslightning = false, material = "models/props_pipes/guttermetal01a", skin = 0, bodygroup = {} },
	["fracture+++"] = { type = "Model", model = "models/props_wasteland/laundry_washer001a.mdl", bone = "v_weapon.M3_PARENT", rel = "fracture", pos = Vector(-1.5, 0, -1), angle = Angle(0, -90, -90), size = Vector(0.035, 0.029, 0.25), color = Color(69, 77, 94, 255), surpresslightning = false, material = "models/props_pipes/valve001_skin2", skin = 0, bodygroup = {} },
	["fracture++++"] = { type = "Model", model = "models/props_c17/utilityconnecter003.mdl", bone = "v_weapon.M3_PARENT", rel = "fracture", pos = Vector(4.675, 0, -0.201), angle = Angle(0, -90, 0), size = Vector(0.625, 0.625, 0.625), color = Color(70, 87, 104, 255), surpresslightning = false, material = "models/props_pipes/valve001_skin2", skin = 0, bodygroup = {} },
	["fracture"] = { type = "Model", model = "models/props_wasteland/laundry_washer003.mdl", bone = "v_weapon.M3_PARENT", rel = "", pos = Vector(0, -4.5, -10.91), angle = Angle(90, -90, 0), size = Vector(0.25, 0.039, 0.029), color = Color(69, 90, 123, 255), surpresslightning = false, material = "models/props_pipes/guttermetal01a", skin = 0, bodygroup = {} },
	["fracture++"] = { type = "Model", model = "models/props_c17/factorymachine01.mdl", bone = "v_weapon.M3_PARENT", rel = "fracture", pos = Vector(-5.715, 0, -1.4), angle = Angle(180, -90, 0), size = Vector(0.025, 0.025, 0.045), color = Color(75, 100, 125, 255), surpresslightning = false, material = "models/props_pipes/guttermetal01a", skin = 0, bodygroup = {} },
	["fracture+"] = { type = "Model", model = "models/props_c17/traffic_light001a.mdl", bone = "v_weapon.M3_PARENT", rel = "fracture", pos = Vector(-5.715, 0, 0), angle = Angle(0, 0, 90), size = Vector(1.399, 0.1, 0.119), color = Color(75, 100, 125, 255), surpresslightning = false, material = "models/props_pipes/guttermetal01a", skin = 0, bodygroup = {} },
	["fracture++++++"] = { type = "Model", model = "models/props_c17/trappropeller_lever.mdl", bone = "v_weapon.M3_PARENT", rel = "fracture", pos = Vector(10.5, 0, -2), angle = Angle(0, -90, -33.896), size = Vector(1, 0.6, 1.2), color = Color(87, 109, 117, 255), surpresslightning = false, material = "models/props_pipes/guttermetal01a", skin = 0, bodygroup = {} },
	["fracture+++++"] = { type = "Model", model = "models/props_c17/utilityconnecter002.mdl", bone = "v_weapon.M3_PARENT", rel = "fracture", pos = Vector(8, 0, -2.597), angle = Angle(0, -90, 180), size = Vector(0.2, 0.2, 0.2), color = Color(70, 87, 104, 255), surpresslightning = false, material = "models/props_pipes/valve001_skin2", skin = 0, bodygroup = {} }
}

SWEP.WElements = {
	["fracture+++"] = { type = "Model", model = "models/props_wasteland/laundry_washer001a.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "fracture", pos = Vector(-1.5, 0, -1), angle = Angle(0, -90, -90), size = Vector(0.035, 0.029, 0.25), color = Color(69, 77, 94, 255), surpresslightning = false, material = "models/props_pipes/valve001_skin2", skin = 0, bodygroup = {} },
	["fracture+"] = { type = "Model", model = "models/props_c17/traffic_light001a.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "fracture", pos = Vector(-5.715, 0, 0), angle = Angle(0, 0, 90), size = Vector(1.399, 0.1, 0.119), color = Color(75, 100, 125, 255), surpresslightning = false, material = "models/props_pipes/guttermetal01a", skin = 0, bodygroup = {} },
	["fracture"] = { type = "Model", model = "models/props_wasteland/laundry_washer003.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "", pos = Vector(16, 1, -4.301), angle = Angle(180, 0, 0), size = Vector(0.25, 0.039, 0.029), color = Color(69, 77, 94, 255), surpresslightning = false, material = "models/props_pipes/guttermetal01a", skin = 0, bodygroup = {} },
	["fracture++++"] = { type = "Model", model = "models/props_c17/utilityconnecter003.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "fracture", pos = Vector(4.675, 0, -0.201), angle = Angle(0, -90, 0), size = Vector(0.625, 0.625, 0.625), color = Color(70, 87, 104, 255), surpresslightning = false, material = "models/props_pipes/valve001_skin2", skin = 0, bodygroup = {} },
	["fracture+++++"] = { type = "Model", model = "models/props_c17/utilityconnecter002.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "fracture", pos = Vector(8, 0, -2.597), angle = Angle(0, -90, 180), size = Vector(0.2, 0.2, 0.2), color = Color(70, 87, 104, 255), surpresslightning = false, material = "models/props_pipes/valve001_skin2", skin = 0, bodygroup = {} },
	["fracture+++++++"] = { type = "Model", model = "models/items/combine_rifle_cartridge01.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "fracture", pos = Vector(0, 0, 0.25), angle = Angle(97.013, 0, 0), size = Vector(0.6, 0.4, 1.598), color = Color(77, 100, 125, 255), surpresslightning = false, material = "models/props_pipes/guttermetal01a", skin = 0, bodygroup = {} },
	["fracture++++++"] = { type = "Model", model = "models/props_c17/trappropeller_lever.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "fracture", pos = Vector(10.5, 0, -2), angle = Angle(0, -90, -33.896), size = Vector(1, 0.6, 1.2), color = Color(87, 99, 107, 255), surpresslightning = false, material = "models/props_pipes/guttermetal01a", skin = 0, bodygroup = {} },
	["fracture++"] = { type = "Model", model = "models/props_c17/factorymachine01.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "fracture", pos = Vector(-5.715, 0, -1.4), angle = Angle(180, -90, 0), size = Vector(0.025, 0.025, 0.045), color = Color(75, 100, 125, 255), surpresslightning = false, material = "models/props_pipes/guttermetal01a", skin = 0, bodygroup = {} }
}