--General--
SWEP.PrintName		= "'Spinfusor' Pulse Disc Launcher"
SWEP.Purpose		= "Launches pulse projectiles that react on walls, sending energy back in the direction they travelled."
SWEP.Slot			= 3
SWEP.SlotPos		= 0
SWEP.Category		= "TFA ZS"
SWEP.Author			= "Global, JetBoom, Raox"
SWEP.Spawnable		= true
SWEP.AdminSpawnable	= true

sound.Add(
{
	name = "Weapon_Slayer.Single",
	channel = CHAN_AUTO,
	volume = 1,
	soundlevel = 100,
	pitch = {125, 135},
	sound = {"weapons/physcannon/superphys_launch2.wav", "weapons/physcannon/superphys_launch3.wav"}
})

SWEP.Base = "tfa_gun_base"

--Model and Animations--
SWEP.HoldType = "crossbow"
SWEP.ViewModel = "models/weapons/c_crossbow.mdl"
SWEP.WorldModel = "models/weapons/w_irifle.mdl"
SWEP.UseHands = true
SWEP.ViewModelFOV = 62
SWEP.ViewModelFlip = false
SWEP.ShowViewModel = true
SWEP.ShowWorldModel = false
SWEP.DisableIdleAnimations	= true
SWEP.ForceDryFireOff		= true
SWEP.DoMuzzleFlash			= true
SWEP.MuzzleAttachment		= "muzzle"
SWEP.MuzzleFlashEffect		= "tfa_muzzleflash_cryo"
SWEP.SequenceEnabled = { [ACT_VM_FIDGET] = false }
SWEP.MaterialTable = {
    [1] = "nodraw",
    [2] = "nodraw",
	[3] = "nodraw"
}

--Sight Positions--
SWEP.data						= {}
SWEP.data.ironsights			= 0
SWEP.Secondary.IronFOV			= 65

SWEP.IronSightsPos = Vector(0, 0, 0)
SWEP.IronSightsAng = Vector(0, 0, 0)

SWEP.RunSightsPos = Vector(4, -1, 0)
SWEP.RunSightsAng = Vector(-12, 25, 0)

SWEP.NZPaPName			= "Frostbite"	-- What name this weapon should use when Pack-a-Punched.

function SWEP:OnPaP()
	self.Primary.Damage = self.Primary.Damage*2.5
	self.Primary.ClipSize = 7
	self.Primary.ClipSize_Orig = 7
	self.Primary.ClipSize_ORIG_REAL = 7
	self.pap = true
	self.Ispackapunched = 1
	self.Primary.ClipSize = 21
	self.Primary.DefaultClip = 231
	self.Primary.MaxAmmo = 231
	return true
end

--Projectiles and gun behavior--
SWEP.ProjectileEntity			= "proj_disc"
SWEP.ProjectileVelocity			= 1500
SWEP.ProjectileModel			= nil
SWEP.Primary.Sound				= Sound("Weapon_Slayer.Single")
SWEP.Primary.Ammo				= "AR2"
SWEP.Primary.Damage				= 125 -- only shown in C menu
SWEP.Primary.AmmoConsumption	= 7
SWEP.Primary.ClipSize			= 7
SWEP.Primary.DefaultClip		= 105
SWEP.DisableChambering			= true
SWEP.Primary.Spread				= 0.001
SWEP.EventTable = {
	[ACT_VM_RELOAD] = {
		{ ["time"] = 0, ["type"] = "sound", ["value"] = Sound("weapons/smg1/smg1_reload.wav") }
	}
}
util.PrecacheSound("weapons/smg1/smg1_reload.wav")

--Model Elements--
SWEP.VElements = {
	["base+++"] = { type = "Model", model = "models/props_lab/eyescanner.mdl", bone = "ValveBiped.Crossbow_base", rel = "", pos = Vector(-0, -1.854, 0.455), angle = Angle(-24.781, 90, 180), size = Vector(0.307, 0.476, 0.423), color = Color(103, 168, 255, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
	["base+++++"] = { type = "Model", model = "models/props_combine/combine_binocular01.mdl", bone = "ValveBiped.Crossbow_base", rel = "", pos = Vector(-0, 1.748, -1.219), angle = Angle(-9.438, 90, -0), size = Vector(0.316, 0.428, 0.321), color = Color(255, 255, 255, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
	["base"] = { type = "Model", model = "models/props_combine/combine_fence01b.mdl", bone = "ValveBiped.Crossbow_base", rel = "", pos = Vector(-3.401, -0.851, 6.88), angle = Angle(0, -90, 0), size = Vector(0.174, 0.174, 0.174), color = Color(142, 197, 255, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
	["base++++"] = { type = "Model", model = "models/props_combine/combinecrane002.mdl", bone = "ValveBiped.Crossbow_base", rel = "", pos = Vector(0.391, -2.362, 14.17), angle = Angle(-180, 0, 0), size = Vector(0.032, 0.029, 0.067), color = Color(125, 193, 255, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
	["base+"] = { type = "Model", model = "models/props_combine/combine_fence01a.mdl", bone = "ValveBiped.Crossbow_base", rel = "", pos = Vector(3.4, -0.851, 6.88), angle = Angle(0, -90, 0), size = Vector(0.174, 0.174, 0.174), color = Color(142, 197, 255, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
	["base++"] = { type = "Model", model = "models/props_combine/breentp_rings.mdl", bone = "ValveBiped.bolt", rel = "", pos = Vector(0, -0.468, 1.162), angle = Angle(0, 0, 90), size = Vector(0.054, 0.054, 0.014), color = Color(42, 173, 255, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} }
}

SWEP.WElements = {
	["base+++++"] = { type = "Model", model = "models/props_combine/combine_binocular01.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "", pos = Vector(7.879, 1.001, -0.686), angle = Angle(-100.002, 7.423, 0), size = Vector(0.316, 0.428, 0.321), color = Color(255, 255, 255, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
	["base++"] = { type = "Model", model = "models/props_combine/breentp_rings.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "", pos = Vector(13.13, -0.075, -5.329), angle = Angle(0, 0, 0), size = Vector(0.054, 0.054, 0.014), color = Color(42, 173, 255, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
	["base+"] = { type = "Model", model = "models/props_combine/combine_fence01a.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "", pos = Vector(15.503, 3.358, -4.2), angle = Angle(90, -170, 0), size = Vector(0.174, 0.174, 0.174), color = Color(142, 197, 255, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
	["base++++"] = { type = "Model", model = "models/props_combine/combinecrane002.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "", pos = Vector(22.2, -1.374, -5.395), angle = Angle(180, -79.429, 90), size = Vector(0.032, 0.029, 0.067), color = Color(125, 193, 255, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
	["base+++"] = { type = "Model", model = "models/props_lab/eyescanner.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "", pos = Vector(8.237, 0.861, -5.441), angle = Angle(-57.536, -171.206, 0), size = Vector(0.307, 0.476, 0.423), color = Color(103, 168, 255, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
	["base"] = { type = "Model", model = "models/props_combine/combine_fence01b.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "", pos = Vector(14.208, -3.931, -4.16), angle = Angle(90, -170, 0), size = Vector(0.174, 0.174, 0.174), color = Color(142, 197, 255, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} }
}