-- Variables that are used on both client and server
SWEP.Category				= "TFA ZS"
SWEP.Author				= "Global, JetBoom, Raox"
SWEP.Contact				= ""
SWEP.Purpose				= "A cloud of strength and defence boosting gas affecting all humans in the radius, also providing a small amount of armor."
SWEP.Instructions				= ""
SWEP.PrintName				= "Bloodshot Bomb"		-- Weapon name (Shown on HUD)	
SWEP.Slot				= 4				-- Slot in the weapon selection menu
SWEP.SlotPos				= 40			-- Position in the slot
SWEP.DrawAmmo				= true		-- Should draw the default HL2 ammo counter
SWEP.DrawWeaponInfoBox			= false		-- Should draw the weapon info box
SWEP.BounceWeaponIcon   		= 	false	-- Should the weapon icon bounce?
SWEP.DrawCrosshair			= false		-- set false if you want no crosshair
SWEP.Weight				= 2			-- rank relative ot other weapons. bigger is better
SWEP.AutoSwitchTo			= true		-- Auto switch to if we pick it up
SWEP.AutoSwitchFrom			= true		-- Auto switch from if you pick up a better weapon
SWEP.HoldType 				= "grenade"		-- how others view you carrying the weapon
-- normal melee melee2 fist knife smg ar2 pistol rpg physgun grenade shotgun crossbow slam passive 
-- you're mostly going to use ar2, smg, shotgun or pistol. rpg and ar2 make for good sniper rifles

SWEP.ViewModel			= "models/weapons/c_grenade.mdl"
SWEP.ShowViewModel		= true
SWEP.WorldModel			= "models/weapons/w_grenade.mdl"
SWEP.ShowWorldModel			= false
SWEP.Base				= "tfa_nade_base"
SWEP.Spawnable				= true
SWEP.UseHands = true
SWEP.AdminSpawnable			= true

SWEP.ViewModelFlip = false
SWEP.ViewModelFOV = 70

SWEP.MaterialTable = {
    [1] = "nodraw",
	[2] = "nodraw",
	[3] = "nodraw"
}

SWEP.VElements = {
	["corrosive_nade++"] = { type = "Model", model = "models/healthvial.mdl", bone = "ValveBiped.Grenade_body", rel = "corrosive_nade", pos = Vector(0, -7, 0.4), angle = Angle(0, 0, 90), size = Vector(0.75, 0.75, 1.299), color = Color(255, 59, 0, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
	["corrosive_nade+"] = { type = "Model", model = "models/props_wasteland/laundry_washer003.mdl", bone = "ValveBiped.Grenade_body", rel = "corrosive_nade", pos = Vector(0, -0.201, 0.4), angle = Angle(0, 90, 0), size = Vector(0.119, 0.019, 0.019), color = Color(100, 100, 100, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
	["corrosive_nade"] = { type = "Model", model = "models/props_lab/labpart.mdl", bone = "ValveBiped.Grenade_body", rel = "", pos = Vector(0, 0, -1), angle = Angle(0, 0, 90), size = Vector(0.449, 0.55, 0.5), color = Color(200, 200, 200, 255), surpresslightning = false, material = "models/weapons/v_smg1/v_smg1_sheet", skin = 0, bodygroup = {} }
}

SWEP.WElements = {
	["corrosive_nade+"] = { type = "Model", model = "models/props_wasteland/laundry_washer003.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "corrosive_nade", pos = Vector(0, -0.201, 0.4), angle = Angle(0, 90, 0), size = Vector(0.119, 0.019, 0.019), color = Color(100, 100, 100, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
	["corrosive_nade++"] = { type = "Model", model = "models/healthvial.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "corrosive_nade", pos = Vector(0, -7, 0.4), angle = Angle(0, 0, 90), size = Vector(0.75, 0.75, 1.299), color = Color(255, 59, 0, 255), surpresslightning = false, material = "", skin = 0, bodygroup = {} },
	["corrosive_nade"] = { type = "Model", model = "models/props_lab/labpart.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "", pos = Vector(3.5, 2, 0), angle = Angle(0, 0, 99.35), size = Vector(0.449, 0.55, 0.5), color = Color(200, 200, 200, 255), surpresslightning = false, material = "models/weapons/v_smg1/v_smg1_sheet", skin = 0, bodygroup = {} }
}

if CLIENT then
	language.Add("Gravity_ammo", "Bloodshot Bombs")
end

SWEP.Primary.RPM				= 30		-- This is in Rounds Per Minute
SWEP.Primary.ClipSize			= 1		-- Size of a clip
SWEP.Primary.DefaultClip		= 4		-- Bullets you start with
SWEP.Primary.Automatic			= false		-- Automatic = true; Semi Auto = false
SWEP.Primary.Ammo			= "Gravity"				
-- pistol, 357, smg1, ar2, buckshot, slam, SniperPenetratedRound, AirboatGun
-- Pistol, buckshot, and slam always ricochet. Use AirboatGun for a metal peircing shotgun slug

SWEP.Primary.Round 			= ("proj_bloodbomb")	--NAME OF ENTITY GOES HERE

SWEP.Velocity = 800 -- Entity Velocity
SWEP.Velocity_Underhand = 450 -- Entity Velocity

SWEP.Offset = { --Procedural world model animation, defaulted for CS:S purposes.
        Pos = {
        Up = -1.5,
        Right = 1,
        Forward = 3,
        },
        Ang = {
        Up = -1,
        Right = -2,
        Forward = 178
        },
		Scale = 1.2
}