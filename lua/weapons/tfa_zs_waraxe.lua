--General--
SWEP.PrintName		= "'Waraxe' Handgun"
SWEP.Purpose		= "A modified handgun that fires 3 bullets at once."
SWEP.Slot			= 1
SWEP.SlotPos		= 0
SWEP.Category		= "TFA ZS"
SWEP.Author			= "Global, JetBoom, Raox"
SWEP.Spawnable		= true
SWEP.AdminSpawnable	= true

sound.Add(
{
	name = "Weapon_WarAxe.Single",
	channel = CHAN_WEAPON,
	volume = 0.7,
	soundlevel = 100,
	pitch = {78,82},
	sound = {")weapons/usp/usp_unsil-1.wav"}
})

SWEP.Base = "tfa_gun_base"

--Model and Animations--
SWEP.HoldType = "pistol"
SWEP.ViewModel = "models/weapons/cstrike/c_pist_glock18.mdl"
SWEP.WorldModel = "models/weapons/w_pist_glock18.mdl"
SWEP.UseHands = true
SWEP.ViewModelFOV = 50
SWEP.ViewModelFlip = false
SWEP.ShowViewModel = true
SWEP.ShowWorldModel = true
SWEP.DisableIdleAnimations	= true
SWEP.ForceDryFireOff		= true
SWEP.DoMuzzleFlash			= true
SWEP.MuzzleAttachment		= "1"
SWEP.MuzzleFlashEffect		= nil
SWEP.SequenceEnabled = { /*[ACT_VM_NAME] = boolean*/ }
SWEP.MaterialTable = {
    --[1] = "nodraw",
    --[2] = "nodraw",
	--[3] = "nodraw"
}

--Sight Positions--
SWEP.data						= {}
SWEP.data.ironsights			= 1
SWEP.Secondary.IronFOV			= 65

SWEP.IronSightsPos = Vector(-5.75, 10, 2.7)
SWEP.IronSightsAng = Vector(0, 0, 0)

SWEP.RunSightsPos = Vector(4, -1, 0)
SWEP.RunSightsAng = Vector(-12, 25, 0)

--Projectiles--
SWEP.ProjectileEntity			= nil
SWEP.ProjectileVelocity			= 1500
SWEP.ProjectileModel			= nil

SWEP.NZPaPName			= "Starbright"	-- What name this weapon should use when Pack-a-Punched.

function SWEP:OnPaP()
self.Primary.Damage = self.Primary.Damage*2.5
self.Primary.ClipSize = 9
self.Primary.ClipSize_Orig = 9
self.Primary.ClipSize_ORIG_REAL = 9
self.pap = true
end

--Gun Behavior---
SWEP.Primary.Sound				= Sound("Weapon_WarAxe.Single")
SWEP.Primary.Ammo				= "pistol"
SWEP.Primary.Damage				= 14 -- only shown in C menu
SWEP.Primary.NumShots			= 3
SWEP.Primary.AmmoConsumption	= 1
SWEP.Primary.ClipSize			= 9
SWEP.Primary.DefaultClip		= 81
SWEP.Primary.Automatic			= false
SWEP.DisableChambering			= false
SWEP.Primary.RPM				= 240

--Accuracy--
SWEP.Primary.KickUp					= 0.4
SWEP.Primary.KickDown				= 0.2
SWEP.Primary.KickHorizontal			= 0.5
SWEP.Primary.StaticRecoilFactor		= 0.3
SWEP.Primary.SpreadMultiplierMax	= 4
SWEP.Primary.SpreadIncrement		= 0.18
SWEP.Primary.SpreadRecovery			= 2
SWEP.Primary.Spread					= 0.05
SWEP.Primary.IronAccuracy			= 0.035

--Effects--
SWEP.TracerName					= nil
SWEP.TracerCount				= 1
--SWEP.EventTable = {
--	[ACT_VM_RELOAD] = {
--		{ ["time"] = 0, ["type"] = "sound", ["value"] = Sound("weapons/smg1/smg1_reload.wav") }
--	}
--}
util.PrecacheSound(")weapons/usp/usp_unsil-1.wav")

--Model Elements--
SWEP.VElements = {
	["barrel"] = { type = "Model", model = "models/props_phx/wheels/drugster_front.mdl", bone = "ValveBiped.Bip01_Spine4", rel = "base", pos = Vector(0, 0, -4.448), angle = Angle(180, 0, 0), size = Vector(0.019, 0.019, 0.025), color = Color(255, 255, 255, 255), surpresslightning = false, material = "models/props_combine/metal_combinebridge001", skin = 0, bodygroup = {} },
	["base"] = { type = "Model", model = "models/props_trainstation/pole_448Connection002b.mdl", bone = "v_weapon.Glock_Slide", rel = "", pos = Vector(3.292, 0.305, -0.005), angle = Angle(13.053, -90.301, 89.9), size = Vector(0.085, 0.045, 0.02), color = Color(255, 255, 255, 255), surpresslightning = false, material = "models/props_combine/metal_combinebridge001", skin = 0, bodygroup = {} },
	["sides"] = { type = "Model", model = "models/props_trainstation/Column_Arch001a.mdl", bone = "ValveBiped.Bip01_Spine4", rel = "base", pos = Vector(-0.06, 0, -0.08), angle = Angle(0, 0, 0), size = Vector(0.119, 0.013, 0.09), color = Color(255, 255, 255, 255), surpresslightning = false, material = "models/props_combine/metal_combinebridge001", skin = 0, bodygroup = {} }
}

SWEP.WElements = {
	["barrel"] = { type = "Model", model = "models/props_phx/wheels/drugster_front.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "base", pos = Vector(0, 0, -4.448), angle = Angle(180, 0, 0), size = Vector(0.019, 0.019, 0.025), color = Color(255, 255, 255, 255), surpresslightning = false, material = "models/props_combine/metal_combinebridge001", skin = 0, bodygroup = {} },
	["base"] = { type = "Model", model = "models/props_trainstation/pole_448Connection002b.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "", pos = Vector(6, 1.71, -3.711), angle = Angle(87.421, -5.053, 0), size = Vector(0.085, 0.045, 0.02), color = Color(255, 255, 255, 255), surpresslightning = false, material = "models/props_combine/metal_combinebridge001", skin = 0, bodygroup = {} },
	["sides"] = { type = "Model", model = "models/props_trainstation/Column_Arch001a.mdl", bone = "ValveBiped.Bip01_R_Hand", rel = "base", pos = Vector(-0.06, 0, -0.072), angle = Angle(0, 0, 0), size = Vector(0.119, 0.013, 0.09), color = Color(255, 255, 255, 255), surpresslightning = false, material = "models/props_combine/metal_combinebridge001", skin = 0, bodygroup = {} }
}